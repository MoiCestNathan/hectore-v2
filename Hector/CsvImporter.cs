﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

///<summary>
/// Classe permettant d'importer des données depuis un fichier CSV dans une base de données SQLite
///</summary>
///<remarks>
/// Cette classe est utilisée pour importer des données depuis un fichier CSV dans une base de données SQLite.
///</remarks>
namespace Hector
{
    ///<summary>
    /// Cette classe permet d'importer des données depuis un fichier CSV dans une base de données SQLite
    ///</summary>
    public class CsvImporter
    {
        public event EventHandler<int> ProgressChanged;

        private DatabaseManager _DbManager;

        ///<summary>
        /// Constructeur de la classe CsvImporter
        ///</summary>
        ///<param name="DbManager">Le gestionnaire de base de données à utiliser pour insérer les données</param>
        public CsvImporter(DatabaseManager DbManager)
        {
            _DbManager = DbManager;
        }

        ///<summary>
        /// Méthode permettant de déclencher l'événement ProgressChanged
        ///</summary>
        ///<param name="Progress">La progression actuelle de l'importation</param>
        protected virtual void OnProgressChanged(int Progress)
        {
            ProgressChanged?.Invoke(this, Progress);
        }

        ///<summary>
        /// Méthode permettant d'importer des données depuis un fichier CSV dans une base de données SQLite
        ///</summary>
        ///<param name="FilePath">Le chemin du fichier CSV à importer</param>
        public void ImportCsv(string FilePath)
        {
            int TotalLines = File.ReadAllLines(FilePath).Length;

            int UpdateInterval = 100;

            int ProcessedLines = 0;

            using (var Reader = new StreamReader(FilePath, Encoding.UTF8))
            {
                while (!Reader.EndOfStream)
                {
                    var Line = Reader.ReadLine();

                    ProcessedLines++;

                    var Values = Line.Split(';');

                    // Ignorer les lignes vides ou un en-tête
                    if (String.IsNullOrWhiteSpace(Line) || Line.StartsWith("Description") || Values.Length < 6)
                    {
                        Console.WriteLine($"Ligne ignorée : {Line}");

                        continue;
                    }

                    if (Values.Length != 6)
                    {
                        Console.WriteLine($"Ligne ignorée car elle ne contient pas le bon nombre de colonnes : {Line}");

                        continue;
                    }

                    try
                    {
                        string Description = Values[0];

                        string RefArticle = Values[1];

                        string Marque = Values[2];

                        string Famille = Values[3];

                        string SousFamille = Values[4];

                        float PrixHT = float.Parse(Values[5], CultureInfo.GetCultureInfo("fr-FR"));

                        float PrixArrondi = (float)Math.Round(PrixHT, 2);

                        int Quantite = 1;

                        // Vérifier/insérer et récupérer les références pour les marques, familles, et sous-familles
                        int RefMarque = _DbManager.EnsureMarqueExists(Marque);

                        int RefFamille = _DbManager.EnsureFamilleExists(Famille);

                        int RefSousFamille = _DbManager.EnsureSousFamilleExists(SousFamille, RefFamille);

                        // Insérer les données dans la table Articles avec les références appropriées
                        _DbManager.InsertArticle(RefArticle, Description, RefSousFamille, RefMarque, PrixArrondi, Quantite);

                        Console.WriteLine(PrixArrondi);

                        if (ProcessedLines % UpdateInterval == 0)
                        {
                            int ProgressPercentage = (int)(((double)ProcessedLines / TotalLines) * 100);

                            OnProgressChanged(ProgressPercentage);
                        }

                    }
                    catch (Exception Ex)
                    {
                        Console.WriteLine($"Erreur lors de l'importation de la ligne : {Line}. Détails de l'erreur : {Ex.Message}");
                    }
                }
                OnProgressChanged(100);
            }
        }
    }
}
