﻿
using Hector.model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Windows.Forms;

/// <summary>
/// Représente la fenêtre d'un article.
/// </summary>
namespace Hector
{
    /// <summary>
    /// Fenêtre d'un article.
    /// </summary>
    public partial class FenetreArticle : Form
    {
        private Article articleOriginal;

        /// <summary>
        /// Constructeur de la fenêtre.
        /// </summary>
        public FenetreArticle()
        {
            InitializeComponent();
            this.Shown += new System.EventHandler(this.FenetreArticle_Shown);
        }

        public FenetreArticle(Article article = null)
        {
            InitializeComponent();

            this.Shown += new System.EventHandler(this.FenetreArticle_Shown);

            articleOriginal = article;
            if (article != null)
            {
                TextBoxRef.Text = article.RefArticle;
                TextBoxDesc.Text = article.Description;
                ComboBoxMarq.SelectedItem = article.Marque.Nom;
                ComboBoxFam.SelectedItem = article.Famille.Nom;
                ComboBoxSsFam.SelectedItem = article.SousFamille.Nom;
                NumericUpDownPrix.Value = (decimal)article.PrixHT;
                NumericUpDownQt.Value = article.Quantite;
            }
        }

        private void ButtonValider_Click(object sender, EventArgs e)
        {
            // Récupérez les valeurs des contrôles
            string refArticle = TextBoxRef.Text;
            string description = TextBoxDesc.Text;
            string nomMarque = ComboBoxMarq.SelectedItem.ToString();
            string nomFamille = ComboBoxFam.SelectedItem.ToString();
            string nomSousFamille = ComboBoxSsFam.SelectedItem.ToString();
            float prixHT = (float)NumericUpDownPrix.Value;
            int quantite = (int)NumericUpDownQt.Value;

            // Vérifier que les champs nécessaires sont remplis
            if (string.IsNullOrEmpty(refArticle) || string.IsNullOrEmpty(description) ||
                string.IsNullOrEmpty(nomMarque) || string.IsNullOrEmpty(nomFamille) ||
                string.IsNullOrEmpty(nomSousFamille))
            {
                MessageBox.Show("Veuillez remplir tous les champs.");
                return;
            }

            // Créez des instances des classes Marque, Famille, et SousFamille
            Marque marque = new Marque { Nom = nomMarque };
            Famille famille = new Famille { Nom = nomFamille };
            SousFamille sousFamille = new SousFamille { Nom = nomSousFamille };

            // Créez une instance de la classe Article
            Article article = new Article
            {
                RefArticle = refArticle,
                Description = description,
                Marque = marque,
                Famille = famille,
                SousFamille = sousFamille,
                PrixHT = prixHT,
                Quantite = quantite
            };
            // Initialisez la connexion à la base de données
            string SolutionDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            string DatabaseFileName = "Hector.SQLite";
            string AbsolutePath = Path.Combine(SolutionDirectory, "ExternalFiles", DatabaseFileName);
            string ConnectionString = $"Data Source={AbsolutePath};Version=3;Charset=utf8;";
            DatabaseManager dbManager = new DatabaseManager(ConnectionString);

            Console.WriteLine("Test");

            try
            { 
                if (articleOriginal == null) // Ajout d'un nouvel article
                {
                    Console.WriteLine("Test1");
                    dbManager.InsertArticleManuel(article);
                }
                else // Mise à jour d'un article existant
                {
                    Console.WriteLine("Test2");
                    dbManager.UpdateArticleManuel(article);
                    Console.WriteLine("Test3");
                }

                MessageBox.Show("L'article a été enregistré avec succès.");
                this.Close(); // Fermez la fenêtre après l'ajout/modification
            }
            catch (Exception ex)
            {
                Console.WriteLine("Test4");
                MessageBox.Show($"Erreur lors de l'enregistrement de l'article : {ex.Message}");
            }
            
        }

        private void FenetreArticle_Load(object sender, EventArgs e)
        {
        }

        public List<Marque> ObtenirLesMarques()
        {
            List<Marque> marques = new List<Marque>();

            // Configuration de la chaîne de connexion
            string SolutionDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            string DatabaseFileName = "Hector.SQLite";
            string AbsolutePath = Path.Combine(SolutionDirectory, "ExternalFiles", DatabaseFileName);
            string ConnectionString = $"Data Source={AbsolutePath};Version=3;Charset=utf8;";

            string query = "SELECT Nom FROM Marques";

            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    using (SQLiteDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            marques.Add(new Marque { Nom = reader.GetString(0) });
                        }
                    }
                }
            }
            return marques;
        }

        public List<Famille> ObtenirLesFamilles()
        {
            List<Famille> familles = new List<Famille>();

            // Configuration de la chaîne de connexion
            string SolutionDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            string DatabaseFileName = "Hector.SQLite";
            string AbsolutePath = Path.Combine(SolutionDirectory, "ExternalFiles", DatabaseFileName);
            string ConnectionString = $"Data Source={AbsolutePath};Version=3;Charset=utf8;";

            string query = "SELECT Nom FROM Familles";

            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    using (SQLiteDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            familles.Add(new Famille { Nom = reader.GetString(0) });
                        }
                    }
                }
            }
            return familles;
        }

        public List<SousFamille> ObtenirLesSousFamilles()
        {
            List<SousFamille> sousFamilles = new List<SousFamille>();

            // Configuration de la chaîne de connexion
            string SolutionDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            string DatabaseFileName = "Hector.SQLite";
            string AbsolutePath = Path.Combine(SolutionDirectory, "ExternalFiles", DatabaseFileName);
            string ConnectionString = $"Data Source={AbsolutePath};Version=3;Charset=utf8;";

            string query = "SELECT Nom FROM SousFamilles";

            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    using (SQLiteDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            sousFamilles.Add(new SousFamille { Nom = reader.GetString(0) });
                        }
                    }
                }
            }
            return sousFamilles;
        }

        public void ConfigurerComboBox()
        {
            List<Marque> marques = ObtenirLesMarques();
            foreach (Marque marque in marques)
            {
                ComboBoxMarq.Items.Add(marque.Nom);
            }

            List<Famille> familles = ObtenirLesFamilles();
            foreach (Famille famille in familles)
            {
                ComboBoxFam.Items.Add(famille.Nom);
            }

            List<SousFamille> sousFamilles = ObtenirLesSousFamilles();
            foreach (SousFamille sousFamille in sousFamilles)
            {
                ComboBoxSsFam.Items.Add(sousFamille.Nom);
            }

            if (articleOriginal != null)
            {
                ComboBoxMarq.SelectedIndex = ComboBoxMarq.Items.IndexOf(articleOriginal.SousFamille.Nom.Trim());
                ComboBoxFam.SelectedIndex = ComboBoxFam.Items.IndexOf(articleOriginal.Marque.Nom.Trim());
                ComboBoxSsFam.SelectedIndex = ComboBoxSsFam.Items.IndexOf(articleOriginal.Famille.Nom.Trim());
            }
        }


        private void FenetreArticle_Shown(object sender, EventArgs e)
        {
            ConfigurerComboBox();
        }
    }
}