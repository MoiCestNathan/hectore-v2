﻿
namespace Hector
{
    partial class FenetreFamille
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.LabelNom = new System.Windows.Forms.Label();
            this.LabelRef = new System.Windows.Forms.Label();
            this.TextBoxRef = new System.Windows.Forms.TextBox();
            this.TextBoxNom = new System.Windows.Forms.TextBox();
            this.ButtonValider = new System.Windows.Forms.Button();
            this.TableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.ColumnCount = 2;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.63039F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.36961F));
            this.TableLayoutPanel1.Controls.Add(this.LabelNom, 0, 0);
            this.TableLayoutPanel1.Controls.Add(this.LabelRef, 0, 1);
            this.TableLayoutPanel1.Controls.Add(this.TextBoxRef, 1, 1);
            this.TableLayoutPanel1.Controls.Add(this.TextBoxNom, 1, 0);
            this.TableLayoutPanel1.Location = new System.Drawing.Point(1, -1);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 2;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(441, 201);
            this.TableLayoutPanel1.TabIndex = 0;
            // 
            // LabelNom
            // 
            this.LabelNom.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelNom.AutoSize = true;
            this.LabelNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNom.Location = new System.Drawing.Point(15, 40);
            this.LabelNom.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.LabelNom.Name = "LabelNom";
            this.LabelNom.Size = new System.Drawing.Size(154, 20);
            this.LabelNom.TabIndex = 0;
            this.LabelNom.Text = "Nom de la famille  :";
            // 
            // LabelRef
            // 
            this.LabelRef.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelRef.AutoSize = true;
            this.LabelRef.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelRef.Location = new System.Drawing.Point(15, 140);
            this.LabelRef.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.LabelRef.Name = "LabelRef";
            this.LabelRef.Size = new System.Drawing.Size(101, 20);
            this.LabelRef.TabIndex = 1;
            this.LabelRef.Text = "Référence : ";
            // 
            // TextBoxRef
            // 
            this.TextBoxRef.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TextBoxRef.Location = new System.Drawing.Point(191, 139);
            this.TextBoxRef.Name = "TextBoxRef";
            this.TextBoxRef.Size = new System.Drawing.Size(247, 22);
            this.TextBoxRef.TabIndex = 3;
            // 
            // TextBoxNom
            // 
            this.TextBoxNom.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TextBoxNom.Location = new System.Drawing.Point(191, 39);
            this.TextBoxNom.Name = "TextBoxNom";
            this.TextBoxNom.Size = new System.Drawing.Size(247, 22);
            this.TextBoxNom.TabIndex = 2;
            // 
            // ButtonValider
            // 
            this.ButtonValider.Location = new System.Drawing.Point(291, 206);
            this.ButtonValider.Name = "ButtonValider";
            this.ButtonValider.Size = new System.Drawing.Size(151, 56);
            this.ButtonValider.TabIndex = 1;
            this.ButtonValider.Text = "Valider";
            this.ButtonValider.UseVisualStyleBackColor = true;
            // 
            // FenetreFamille
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 274);
            this.Controls.Add(this.ButtonValider);
            this.Controls.Add(this.TableLayoutPanel1);
            this.MaximumSize = new System.Drawing.Size(460, 321);
            this.MinimumSize = new System.Drawing.Size(460, 321);
            this.Name = "FenetreFamille";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter / Modifier Famille";
            this.TableLayoutPanel1.ResumeLayout(false);
            this.TableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        private System.Windows.Forms.Label LabelNom;
        private System.Windows.Forms.Label LabelRef;
        private System.Windows.Forms.TextBox TextBoxRef;
        private System.Windows.Forms.TextBox TextBoxNom;
        private System.Windows.Forms.Button ButtonValider;
    }
}