﻿using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;

/// <summary>
/// Classe de la fenêtre d'importation de fichiers CSV
/// </summary>
/// <remarks>
/// Cette fenêtre permet d'importer des fichiers CSV dans la base de données SQLite.
/// </remarks>
namespace Hector
{
    /// <summary>
    /// Fenêtre d'importation de fichiers CSV
    /// </summary>
    public partial class FenetreImporter : Form
    {
        private DatabaseManager DbManager;

        /// <summary>
        /// Constructeur de la fenêtre d'importation
        /// </summary>
        public FenetreImporter()
        {
            InitializeComponent();

            string SolutionDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;

            string DatabaseFileName = "Hector.SQLite";

            string AbsolutePath = Path.Combine(SolutionDirectory, "ExternalFiles", DatabaseFileName);

            DbManager = new DatabaseManager(AbsolutePath);
        }

        public FormMain MainForm { get; set; }

        /// <summary>
        /// Gestionnaire de l'événement de clic sur le bouton de sélection de fichier
        /// </summary>
        /// <param name="Sender">Objet déclencheur de l'événement</param>
        /// <param name="Event">Arguments de l'événement</param>
        private void SelectionFichier_Click(object Sender, EventArgs Event)
        {
            OpenFileDialog OpenFileDialog = new OpenFileDialog
            {
                Filter = "CSV Files (*.csv)|*.csv"
            };

            if (OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                string SelectedFilePath = OpenFileDialog.FileName;

                TextBoxFilePathImporter.Text = SelectedFilePath;

                string FileContent = File.ReadAllText(SelectedFilePath, Encoding.UTF8);

                RichTextBox1.Text = FileContent;
            }
        }

        /// <summary>
        /// Gestionnaire de l'événement de clic sur le bouton d'importation
        /// </summary>
        /// <param name="Sender">Objet déclencheur de l'événement</param>
        /// <param name="Event">Arguments de l'événement</param>
        private void AjouterButton_Click(object Sender, EventArgs Event)
        {
            if (string.IsNullOrWhiteSpace(TextBoxFilePathImporter.Text))
            {
                MessageBox.Show("Veuillez sélectionner un fichier CSV à importer.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            // Démarrer l'importation en arrière-plan
            BackgroundWorker1.RunWorkerAsync(TextBoxFilePathImporter.Text);
        }

        /// <summary>
        /// Gestionnaire de l'événement de clic sur le bouton d'annulation
        /// </summary>
        /// <param name="Sender">Objet déclencheur de l'événement</param>
        /// <param name="Event">Arguments de l'événement</param>
        private void BackgroundWorker1_DoWork(object Sender, DoWorkEventArgs Event)
        {
            string FilePath = Event.Argument.ToString();

            CsvImporter CsvImporter = new CsvImporter(DbManager);

            CsvImporter.ProgressChanged += (s, progress) =>
            {
                (Sender as BackgroundWorker)?.ReportProgress(progress);
            };

            CsvImporter.ImportCsv(FilePath);
        }

        /// <summary>
        /// Gestionnaire de l'événement de progression de l'importation
        /// </summary>
        /// <param name="Sender">Objet déclencheur de l'événement</param>
        /// <param name="Event">Arguments de l'événement</param>
        private void BackgroundWorker1_ProgressChanged(object Sender, ProgressChangedEventArgs Event)
        {
            this.BackgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BackgroundWorker1_ProgressChanged);

            Console.WriteLine($"Progression : {Event.ProgressPercentage}%"); // Pour le débogage

            ProgressBarImporter.Value = Event.ProgressPercentage;
        }

        /// <summary>
        /// Gestionnaire de l'événement de fin de l'importation
        /// </summary>
        /// <param name="Sender">Objet déclencheur de l'événement</param>
        /// <param name="Event">Arguments de l'événement</param>
        private void BackgroundWorker1_RunWorkerCompleted(object Sender, RunWorkerCompletedEventArgs Event)
        {
            MessageBox.Show("Importation terminée avec succès.");

            if (MainForm != null)
            {
                MainForm.UpdateMainFormStatusStrip();
            }

            this.Close();
        }

        /// <summary>
        /// Gestionnaire de l'événement de clic sur le bouton d'écrasement
        /// </summary>
        /// <param name="Sender">Objet déclencheur de l'événement</param>
        /// <param name="Event">Arguments de l'événement</param>
        private void EcraserButton_Click(object Sender, EventArgs Event)
        {
            if (MessageBox.Show("Voulez-vous vraiment vider la base de données et importer le nouveau fichier ? Cette action est irréversible.", "Confirmer l'écrasement", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                try
                {
                    DbManager.ViderLaBaseDeDonnees();

                    AjouterButton_Click(Sender, Event);
                }
                catch (Exception Ex)
                {
                    MessageBox.Show($"Une erreur est survenue : {Ex.Message}", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}