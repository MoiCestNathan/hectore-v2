﻿
namespace Hector
{
    partial class FenetreMarque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.LabelMarq = new System.Windows.Forms.Label();
            this.LabelRef = new System.Windows.Forms.Label();
            this.TextBoxNom = new System.Windows.Forms.TextBox();
            this.TextBoxRef = new System.Windows.Forms.TextBox();
            this.ButtonValider = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.84305F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55.15695F));
            this.tableLayoutPanel1.Controls.Add(this.LabelMarq, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.LabelRef, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.TextBoxNom, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.TextBoxRef, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(-1, -1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(446, 218);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // LabelMarq
            // 
            this.LabelMarq.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelMarq.AutoSize = true;
            this.LabelMarq.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMarq.Location = new System.Drawing.Point(15, 44);
            this.LabelMarq.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.LabelMarq.Name = "LabelMarq";
            this.LabelMarq.Size = new System.Drawing.Size(161, 20);
            this.LabelMarq.TabIndex = 0;
            this.LabelMarq.Text = "Nom de la marque : ";
            // 
            // LabelRef
            // 
            this.LabelRef.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelRef.AutoSize = true;
            this.LabelRef.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelRef.Location = new System.Drawing.Point(15, 153);
            this.LabelRef.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.LabelRef.Name = "LabelRef";
            this.LabelRef.Size = new System.Drawing.Size(96, 20);
            this.LabelRef.TabIndex = 1;
            this.LabelRef.Text = "Référence :";
            // 
            // TextBoxNom
            // 
            this.TextBoxNom.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TextBoxNom.Location = new System.Drawing.Point(202, 43);
            this.TextBoxNom.Name = "TextBoxNom";
            this.TextBoxNom.Size = new System.Drawing.Size(241, 22);
            this.TextBoxNom.TabIndex = 2;
            // 
            // TextBoxRef
            // 
            this.TextBoxRef.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TextBoxRef.Location = new System.Drawing.Point(202, 152);
            this.TextBoxRef.Name = "TextBoxRef";
            this.TextBoxRef.Size = new System.Drawing.Size(241, 22);
            this.TextBoxRef.TabIndex = 3;
            // 
            // ButtonValider
            // 
            this.ButtonValider.Location = new System.Drawing.Point(304, 221);
            this.ButtonValider.Name = "ButtonValider";
            this.ButtonValider.Size = new System.Drawing.Size(138, 49);
            this.ButtonValider.TabIndex = 1;
            this.ButtonValider.Text = "Valider";
            this.ButtonValider.UseVisualStyleBackColor = true;
            // 
            // FenetreMarque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 274);
            this.Controls.Add(this.ButtonValider);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximumSize = new System.Drawing.Size(460, 321);
            this.MinimumSize = new System.Drawing.Size(460, 321);
            this.Name = "FenetreMarque";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter / Modifier Marque";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label LabelMarq;
        private System.Windows.Forms.Label LabelRef;
        private System.Windows.Forms.TextBox TextBoxNom;
        private System.Windows.Forms.TextBox TextBoxRef;
        private System.Windows.Forms.Button ButtonValider;
    }
}