﻿
namespace Hector
{
    partial class FenetreSousFamille
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.LabelNomF = new System.Windows.Forms.Label();
            this.LabelNomSF = new System.Windows.Forms.Label();
            this.LabelRef = new System.Windows.Forms.Label();
            this.TextBoxNomSF = new System.Windows.Forms.TextBox();
            this.TextBoxNomF = new System.Windows.Forms.TextBox();
            this.TextBoxRef = new System.Windows.Forms.TextBox();
            this.ButtonValider = new System.Windows.Forms.Button();
            this.TableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.ColumnCount = 2;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.38095F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.61905F));
            this.TableLayoutPanel1.Controls.Add(this.LabelNomF, 0, 1);
            this.TableLayoutPanel1.Controls.Add(this.LabelNomSF, 0, 0);
            this.TableLayoutPanel1.Controls.Add(this.LabelRef, 0, 2);
            this.TableLayoutPanel1.Controls.Add(this.TextBoxNomSF, 1, 0);
            this.TableLayoutPanel1.Controls.Add(this.TextBoxNomF, 1, 1);
            this.TableLayoutPanel1.Controls.Add(this.TextBoxRef, 1, 2);
            this.TableLayoutPanel1.Location = new System.Drawing.Point(1, 1);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 3;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(441, 213);
            this.TableLayoutPanel1.TabIndex = 0;
            // 
            // LabelNomF
            // 
            this.LabelNomF.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelNomF.AutoSize = true;
            this.LabelNomF.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNomF.Location = new System.Drawing.Point(15, 95);
            this.LabelNomF.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.LabelNomF.Name = "LabelNomF";
            this.LabelNomF.Size = new System.Drawing.Size(154, 20);
            this.LabelNomF.TabIndex = 0;
            this.LabelNomF.Text = "Nom de sa famille :";
            // 
            // LabelNomSF
            // 
            this.LabelNomSF.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelNomSF.AutoSize = true;
            this.LabelNomSF.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNomSF.Location = new System.Drawing.Point(15, 25);
            this.LabelNomSF.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.LabelNomSF.Name = "LabelNomSF";
            this.LabelNomSF.Size = new System.Drawing.Size(195, 20);
            this.LabelNomSF.TabIndex = 1;
            this.LabelNomSF.Text = "Nom de la sous famille : ";
            // 
            // LabelRef
            // 
            this.LabelRef.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LabelRef.AutoSize = true;
            this.LabelRef.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelRef.Location = new System.Drawing.Point(15, 166);
            this.LabelRef.Margin = new System.Windows.Forms.Padding(15, 0, 3, 0);
            this.LabelRef.Name = "LabelRef";
            this.LabelRef.Size = new System.Drawing.Size(101, 20);
            this.LabelRef.TabIndex = 2;
            this.LabelRef.Text = "Référence : ";
            // 
            // TextBoxNomSF
            // 
            this.TextBoxNomSF.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TextBoxNomSF.Location = new System.Drawing.Point(233, 24);
            this.TextBoxNomSF.Name = "TextBoxNomSF";
            this.TextBoxNomSF.Size = new System.Drawing.Size(205, 22);
            this.TextBoxNomSF.TabIndex = 3;
            // 
            // TextBoxNomF
            // 
            this.TextBoxNomF.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TextBoxNomF.Location = new System.Drawing.Point(233, 94);
            this.TextBoxNomF.Name = "TextBoxNomF";
            this.TextBoxNomF.Size = new System.Drawing.Size(205, 22);
            this.TextBoxNomF.TabIndex = 4;
            // 
            // TextBoxRef
            // 
            this.TextBoxRef.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TextBoxRef.Location = new System.Drawing.Point(233, 165);
            this.TextBoxRef.Name = "TextBoxRef";
            this.TextBoxRef.Size = new System.Drawing.Size(205, 22);
            this.TextBoxRef.TabIndex = 5;
            // 
            // ButtonValider
            // 
            this.ButtonValider.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ButtonValider.Location = new System.Drawing.Point(313, 220);
            this.ButtonValider.Name = "ButtonValider";
            this.ButtonValider.Size = new System.Drawing.Size(129, 46);
            this.ButtonValider.TabIndex = 1;
            this.ButtonValider.Text = "Valider";
            this.ButtonValider.UseVisualStyleBackColor = true;
            // 
            // FenetreSousFamille
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 274);
            this.Controls.Add(this.ButtonValider);
            this.Controls.Add(this.TableLayoutPanel1);
            this.MaximumSize = new System.Drawing.Size(460, 321);
            this.MinimumSize = new System.Drawing.Size(460, 321);
            this.Name = "FenetreSousFamille";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter / Modifier Sous Famille";
            this.TableLayoutPanel1.ResumeLayout(false);
            this.TableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        private System.Windows.Forms.Label LabelNomF;
        private System.Windows.Forms.Label LabelNomSF;
        private System.Windows.Forms.Label LabelRef;
        private System.Windows.Forms.TextBox TextBoxNomSF;
        private System.Windows.Forms.TextBox TextBoxNomF;
        private System.Windows.Forms.TextBox TextBoxRef;
        private System.Windows.Forms.Button ButtonValider;
    }
}