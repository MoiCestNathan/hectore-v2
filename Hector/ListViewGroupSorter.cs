﻿using System;
using System.Collections;
using System.Windows.Forms;

/// <summary>
/// Classe permettant de trier les groupes d'un ListView
/// </summary>
namespace Hector
{
    /// <summary>
    /// Cette classe permet de trier les groupes d'un ListView
    /// </summary>
    class ListViewGroupSorter : IComparer
    {
        private readonly SortOrder Order;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="SortOrder">Ordre de tri</param>
        public ListViewGroupSorter(SortOrder SortOrder)
        {
            Order = SortOrder;
        }

        /// <summary>
        /// Compare deux objets
        /// </summary>
        /// <param name="X">Premier objet</param>
        /// <param name="Y">Deuxième objet</param>
        /// <returns>Entier indiquant l'ordre de tri</returns>
        public int Compare(object X, object Y)
        {
            ListViewItem ItemX = X as ListViewItem;
            
            ListViewItem ItemY = Y as ListViewItem;

            if (ItemX != null && ItemY != null && ItemX.Group != null && ItemY.Group != null)
            {
                int Result = String.Compare(ItemX.Group.Header, ItemY.Group.Header, StringComparison.Ordinal);
               
                return Order == SortOrder.Ascending ? Result : -Result;
            }
            return 0;
        }
    }
}
