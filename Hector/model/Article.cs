﻿using Hector.model;

namespace Hector
{
    /// <summary>
    /// Classe Article
    /// </summary>
    public class Article
    {
        public string RefArticle { get; set; }
        public string Description { get; set; }
        public Marque Marque { get; set; }
        public Famille Famille { get; set; }
        public SousFamille SousFamille { get; set; }
        public float PrixHT { get; set; }
        public int Quantite { get; set; }

        /// <summary>
        /// Constructeur par défaut de la classe Article
        /// </summary>
        public Article()
        {
        }

        /// <summary>
        /// Constructeur de la classe Article
        /// </summary>
        /// <param name="RefArticle">Référence de l'article</param>
        /// <param name="Description">Description de l'article</param>
        /// <param name="Marque">Marque de l'article</param>
        /// <param name="Famille">Famille de l'article</param>
        /// <param name="SousFamille">Sous-famille de l'article</param>
        /// <param name="PrixHT">Prix hors taxe de l'article</param>
        public Article(string RefArticle, string Description, Marque Marque, Famille Famille, SousFamille SousFamille, float PrixHT)
        {
            this.RefArticle = RefArticle;

            this.Description = Description;

            this.Marque = Marque;

            this.Famille = Famille;

            this.SousFamille = SousFamille;

            this.PrixHT = PrixHT;
        }
    }
}
